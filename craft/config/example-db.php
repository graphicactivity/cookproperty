<?php

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'cookproperty.dev' => array(
    	'database' => 'cookproperty',
        'user' => 'root',
        'password' => 'xxx',
    ),
    'cookproperty.co.nz' => array(
    	'database' => 'cookproperty',
        'user' => 'root',
        'password' => 'xxx',
    ),
);
