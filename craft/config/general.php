<?php

return array(
    '*' => array(
		'siteUrl' => null,
		'defaultWeekStartDay' => 0,
		'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
		'cpTrigger' => 'admin',
        'allowAutoUpdates' => false,
        'generateTransformsBeforePageLoad' => true,
    ),

    'cookproperty.local' => array(
        'devMode' => true,
        'environmentVariables' => array(
        	'basePath' => '/Users/GraphicActivity/Sites/cookproperty/public/',
            'baseUrl'  => 'http://cookproperty.local/',
        )
    ),

    'cookproperty.co.nz' => array(
        'devMode' => false,
        'environmentVariables' => array(
        	'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://cookproperty.co.nz/',
        )
    )
);
